# LIBRARY

import json
from pyspark.sql.functions import udf, input_file_name, split, regexp_extract, col
from pyspark.sql.types import ArrayType, FloatType
from pyspark.ml.feature import StringIndexer
from pyspark.mllib.classification import SVMWithSGD
from pyspark.mllib.regression import LabeledPoint


# 1st UDF: To convert string into array of floats
def parse_embedding_from_string(x):
    res = json.loads(x)
    res = [float(x_) for x_ in res]
    return res


#  retrieve_embedding = udf(parse_embedding_from_string, ArrayType(DoubleType()))
retrieve_embedding = udf(parse_embedding_from_string, ArrayType(FloatType()))

# -----------------SPARK SHELL--------------------------------------------------
# -----------------TEST ON YORKSHIRE_TERRIER  &  ABYSSINIAN------------------------
# YORKSHIRE_TERRIER
# At this stage, we have a df with 1 column, values as array of float
df = spark.read.text("/home/davidhinh/code/p2/images/yorkshire*json")
df_converted = df.withColumn("value", retrieve_embedding(col("value")))
df_converted.printSchema()
df_converted.show()

# Add the column Label. I choose to use regex because of the complexity of the filename to extract (multiple _)
# regex_str extracts everything before the first _number
# Use split function to get the last part after "/" ( 6 is used to obtain the last part)
regex_str = "(.*).+?(?=_[0-9])"
df_labeled = df_converted.withColumn('class', regexp_extract(split(input_file_name(), '/')[6], regex_str, 0))
df_labeled.printSchema()
df_labeled.show()

# ABYSSINIAN
# At this stage, we have a df with 1 column, values as array of float
df2 = spark.read.text("/home/davidhinh/code/p2/images/Abyssinian*json")
df2_converted = df2.withColumn("value", retrieve_embedding(col("value")))
df2_converted.printSchema()
df2_converted.show()

# Add the column Label. I choose to use regex because of the complexity of the filename to extract (multiple _)
# same regex-str
# Use split function to get the last part after "/" ( 6 is used to obtain the last part)
df2_labeled = df2_converted.withColumn('class', regexp_extract(split(input_file_name(), '/')[6], regex_str, 0))
df2_labeled.printSchema()
df2_labeled.show()

# Both df are loaded --> df_labeled (yorkshire_terrier)  &  df2_labeled (Abyssinian)

# Union of the 2 df
uniondf = df_labeled.union(df2_labeled)
uniondf.printSchema()
uniondf.show()

# Assign label to each class --> df_labeled will get 1. df2_labeled will get 0.
indexer = StringIndexer(inputCol="class", outputCol="label")
indexed = indexer.fit(uniondf).transform(uniondf)
indexed.printSchema()
indexed.show()

# LabeledPoint Class represents the features and labels of a data point.
# LabeledPoint(label, features)
labeled = indexed.rdd.map(lambda x: LabeledPoint(x.label, x.value))
train, test = labeled.randomSplit([0.05, 0.95])
model = SVMWithSGD.train(train)
testprediction = test.map(lambda x: (x.label, model.predict(x.features)))

# Accuracy of the model
# testprediction.filter(lambda x: x[1] != x[0]).count()
# train.filter(lambda x: x.label == 1).count()
# testprediction.take(100)

testprediction.filter(lambda x: x[1] == x[0]).count()) / (testprediction.count()
