# David HINH

# Using Spark 3.1.1

# To run the path
# spark-submit PathPythonFile Class1 Class2 FilePath
# For example spark-submit ~/code/p2/pet_classification.py Abyssinian Bombay /home/davidhinh/code/p2/images
# If you want to run only for one class, replace Class1 or Class2 by ""
# For example spark-submit ~/code/p2/pet_classification.py Abyssinian "" /home/davidhinh/code/p2/images

# IMPORTS
import json
import os.path
import sys
from pyspark import SparkContext
from pyspark.mllib.classification import SVMWithSGD
from pyspark.mllib.regression import LabeledPoint
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, input_file_name, lit, regexp_extract, split, udf, when
from pyspark.sql.types import ArrayType, FloatType

sc = SparkContext()
spark = SparkSession.builder.getOrCreate()


# UDF to convert string into array of floats
def parse_embedding_from_string(x):
    res = json.loads(x)
    res = [float(x_) for x_ in res]
    return res


retrieve_embedding = udf(parse_embedding_from_string, ArrayType(FloatType()))


def classification1v1(class1, class2, filepath):
    # Classification One vs One
    path1 = filepath + "/{name1}*json".format(name1=class1)
    path2 = filepath + "/{name2}*json".format(name2=class2)
    # Load the dataframe for class1 (2 columns: values and labels)
    df_class1 = spark.read.text(path1)
    df_class1 = df_class1.withColumn("value", retrieve_embedding(col("value")))
    df_class1 = df_class1.withColumn("label", lit(0))
    # df_class1.printSchema()
    # df_class1.show()

    # Load the dataframe for class2 (2 columns: values and labels)
    df_class2 = spark.read.text(path2)
    df_class2 = df_class2.withColumn("value", retrieve_embedding(col("value")))
    df_class2 = df_class2.withColumn("label", lit(1))
    # df_class2.printSchema()
    # df_class2.show()

    # Merge both dataframes into one
    df_bothclasses = df_class1.union(df_class2)
    # df_bothclasses.printSchema()
    # df_bothclasses.show()

    # Prediction
    df_labeled = df_bothclasses.rdd.map(lambda x: LabeledPoint(x.label, x.value))
    train, test = df_labeled.randomSplit([0.25, 0.75])
    model1v1 = SVMWithSGD.train(train)
    testprediction = test.map(lambda x: (x.label, model1v1.predict(x.features)))

    # Accuracy of the model
    print("There are {success} classifications that have succeeded".format(
        success=testprediction.filter(lambda x: x[1] == x[0]).count()))
    print("There are {fail} classifications that have failed".format(
        fail=testprediction.filter(lambda x: x[1] != x[0]).count()))
    print("The accuracy of the model is about {accuracy}".format(
        accuracy=((testprediction.filter(lambda x: x[1] == x[0]).count()) / (testprediction.count())
                  )))
    print("-------------------------CLASSIFICATION CLASS 1  VS CLASS 2------------------------------------------------")


def classification1vall(oneclass, filepath):
    # Classification One vs All
    path = filepath + "/*json"
    # Load the dataframe of every class
    df_allclass = spark.read.text(path)
    df_allclass = df_allclass.withColumn("value", retrieve_embedding(col("value")))

    # Add the column class
    regex_str = "(.*).+?(?=_[0-9])"
    df_allclass = df_allclass.withColumn('class', regexp_extract(split(input_file_name(), '/')[6], regex_str, 0))
    # df_allclass.printSchema()
    # df_allclass.show()

    # Add column label
    df_final = df_allclass.withColumn("label", (when(col("class") == "{alone}".format(alone=oneclass), 0).otherwise(1)))

    # Prediction
    df_labeled = df_final.rdd.map(lambda x: LabeledPoint(x.label, x.value))
    train, test = df_labeled.randomSplit([0.25, 0.75])
    model1vall = SVMWithSGD.train(train)
    testprediction = test.map(lambda x: (x.label, model1vall.predict(x.features)))

    # Accuracy of the model
    print("There are {success} classifications that have succeeded".format(
        success=testprediction.filter(lambda x: x[1] == x[0]).count()))
    print("There are {fail} classifications that have failed".format(
        fail=testprediction.filter(lambda x: x[1] != x[0]).count()))
    print("The accuracy of the model is about {accuracy}".format(
        accuracy=((testprediction.filter(lambda x: x[1] == x[0]).count()) / (testprediction.count())
                  )))
    print("-------------------------CLASSIFICATION ONE CLASS VS ALL CLASSES-------------------------------------------")


def classification(class1, class2, filepath):
    # Defining paths.
    path1 = filepath + "/{name1}_1.jpg.json".format(name1=class1)
    path2 = filepath + "/{name2}_1.jpg.json".format(name2=class2)
    # Check the path of one specific file
    # Both mentioned classes are in the dataset
    if os.path.exists(path1) == True and os.path.exists(path2) == True:
        classification1v1(class1, class2, filepath)
    # class1 is not in the dataset
    if os.path.exists(path1) == False and os.path.exists(path2) == True:
        classification1vall(class2, filepath)
    # class2 is not in the dataset
    if os.path.exists(path1) == True and os.path.exists(path2) == False:
        classification1vall(class1, filepath)
    # Both classes are not in the dataset
    if os.path.exists(path1) == False and os.path.exists(path2) == False:
        print("-------------------------SORRY, THE MENTIONED CLASSES ARE NOT IN THE DATASET----------------------")


classification(sys.argv[1], sys.argv[2], sys.argv[3])
print("THE ARGUMENTS ARE:", str(sys.argv))
