# David HINH

# Using Spark 3.1.1

# Classification model of cats and dogs using SVMWithSGD
#
# optional arguments:
#   -h, --help        show this help message and exit
#   -c1 , --class1    first pet class for classification
#   -c2 , --class2    second pet class for classification
#   -f , --filepath   path of the file where the features are located

# -c1 path must exist to run the code
# -c2 is optional. If -c2 is none or the path does not exist, classification 1vall will be performed with -c1 argument.

# spark-submit PathPythonFile -c1 ...  -c2 ...  -f ...

# For example
# spark-submit ~/code/p2/pet_classification_argparse.py -c1 Abyssinian -c2 Bombay -f /home/davidhinh/code/p2/images


# IMPORTS
import argparse
import sys
import json
from pyspark import SparkContext
from pyspark.mllib.classification import SVMWithSGD
from pyspark.mllib.regression import LabeledPoint
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, input_file_name, lit, regexp_extract, split, udf, when
from pyspark.sql.types import ArrayType, FloatType

# ARGUMENTS
parser = argparse.ArgumentParser(description='Classification model of cats and dogs')
parser.add_argument("-c1", "--class1", type=str, metavar='', required=True, help="first pet class for classification")
parser.add_argument("-c2", "--class2", type=str, metavar='', help="second pet class for classification")
parser.add_argument("-f", "--filepath", type=str, metavar='', required=True, help="path of the file where the "
                                                                                  "features are located")
args = parser.parse_args()

# SPARK APPLICATION
sc = SparkContext()
spark = SparkSession.builder.getOrCreate()


# UDF to convert string into array of floats
def parse_embedding_from_string(x):
    res = json.loads(x)
    res = [float(x_) for x_ in res]
    return res


retrieve_embedding = udf(parse_embedding_from_string, ArrayType(FloatType()))


def check_class_exists(class_test, filepath):
    try:
        path = filepath + f"/{class_test}*json"
        spark.read.text(path)
    except Exception as e:
        print(f"no data with class {class_test} was found")
        print(e)
        sys.exit(1)


def classification(class1, filepath, class2=None):

    # Check if the path of class1 exists
    check_class_exists(class1, filepath)

    # Load all the data in a df_allclass
    pathall = filepath + "/*json"
    df_allclass = spark.read.text(pathall)
    df_allclass = df_allclass.withColumn("value", retrieve_embedding(col("value")))

    # Add the column class
    regex_str = "(.*).+?(?=_[0-9])"
    df_allclass = df_allclass.withColumn('pet_class', regexp_extract(split(input_file_name(), '/')[6], regex_str, 0))

    # Add the column Label --> 0 for class1 and 1 for the other classes
    df_final = df_allclass.withColumn("label",
                                      (when(col("pet_class") == class1, 0).otherwise(1)))

    # Check if arg -c2 is given by user & path class2 exists
    if class2:
        check_class_exists(class2, filepath)
        # Filter the df to get only a df with the data of both classes
        df_final = df_final.filter((col("pet_class") == class1) | (col("pet_class") == class2))

    #  MODEL
    df_labeled = df_final.rdd.map(lambda x: LabeledPoint(x.label, x.value))
    df_cache = df_labeled.cache()
    train, test = df_cache.randomSplit([0.25, 0.75])
    model = SVMWithSGD.train(train)
    testprediction = test.map(lambda x: (x.label, model.predict(x.features)))
    testprediction_cache = testprediction.cache()

    #  ACCURACY OF THE MODEL
    success = testprediction_cache.filter(lambda x: x[1] == x[0]).count()
    print("There are {success} classifications that have succeeded".format(
        success=success))
    fail = testprediction_cache.filter(lambda x: x[1] != x[0]).count()
    print("There are {fail} classifications that have failed".format(
        fail=fail))
    print("The accuracy of the model is about {accuracy}".format(
        accuracy=success / (success + fail)))


if __name__ == '__main__':

    if args.class2 is None:
        classification(args.class1, args.filepath)
        print("THE ARGUMENTS ARE:", str(args.class1), str(args.class2), str(args.filepath))
    else:
        classification(args.class1, args.filepath, args.class2)
        print("THE ARGUMENTS ARE:", str(args.class1), str(args.filepath), str(args.class2))
